import 'package:flutter/material.dart';
import 'package:flutter_app/pages/CalcularPri.dart';
import 'package:flutter_app/pages/SplashScreen.dart';
import 'pages/TelaInicial.dart';
import 'models/item.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Meus Gastos',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
      
        primarySwatch: Colors.green,
      ),
      home: SplashScreen(),
    );
  }
}


