import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_app/pages/CalcularPri.dart';
import 'package:flutter_app/pages/TelaInicial.dart';
import 'package:intl/intl.dart';


import 'dart:async';


import 'package:flutter_app/pages/TelaInicial.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';

class SplashScreen extends StatefulWidget {
  @override
  _State createState() => _State();
}

class _State extends State<SplashScreen> {
  
final List<String> meses = <String>['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'];
int codmes = 0;
int ano = 2019;

  void _executar() async {

    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = documentsDirectory.path + "/GastosDB.db";


    var database = await openDatabase(path, version: 3,
    onUpgrade: (Database db, int version, int info) async{
       await db.execute("CREATE TABLE IF NOT EXISTS desejos(id_desejos INTEGER PRIMARY KEY AUTOINCREMENT," +
                    " nome_desejos VARCHAR NOT NULL," +
                    " valor_desejos REAL NOT NULL," +
                    " prioridade INTEGER DEFAULT 0)");  
    },
    onCreate: (Database db, int version) async {
      await db.execute("CREATE TABLE IF NOT EXISTS gastos(id_gastos INTEGER PRIMARY KEY AUTOINCREMENT," +
                    " nome VARCHAR NOT NULL," +
                    " valor REAL NOT NULL," +
                    " qtd_parcelas INTEGER DEFAULT 0," +
                    " parcelas_pagas INTEGER DEFAULT 0," +
                    " tipo INTEGER DEFAULT 0," +
                    " fixo INTEGER DEFAULT 0)");      

       await db.execute("CREATE TABLE IF NOT EXISTS mes(id_mes INTEGER PRIMARY KEY AUTOINCREMENT," +
                    " num_mes VARCHAR NOT NULL," +
                    " nome_mes VARCHAR NOT NULL," +
                    " ano INTEGER DEFAULT 0)");      
       await db.execute("CREATE TABLE IF NOT EXISTS gastos_mes(id_gastos_mes INTEGER PRIMARY KEY AUTOINCREMENT," +
                    "mes_id_mes INTEGER NOT NULL," +
                    "gastos_id_gastos INTEGER NOT NULL," +
                    " FOREIGN KEY (mes_id_mes)" +
                     "REFERENCES mes (id_mes)" +
                    " FOREIGN KEY (gastos_id_gastos)" +
                    "REFERENCES gastoss (id_gastos))");    
                    
                     for(int i = 0; i < 120; i++){
      int cod = codmes+1;
      db.execute("INSERT INTO mes(nome_mes, num_mes, ano) VALUES ('${meses[codmes]}', '$cod', $ano)");
      if(codmes == 11){
        codmes = 0;
        ano++;
      }else{
        codmes++;
      }

    }
    }
    
      
    );

    //await database.rawDelete('drop table desejos');

   
    //await database.rawDelete('delete from gastos_mes');

    /*await database.rawDelete('delete from gastos');
    await database.rawDelete('delete from mes');*/
    
    /*await database.execute("INSERT INTO gastos(nome, valor, qtd_parcelas, parcelas_pagas, tipo, fixo) VALUES ('Faculdade', 120, 0, 0, 1, 1)");
    await database.execute("INSERT INTO gastos(nome, valor, qtd_parcelas, parcelas_pagas, tipo, fixo) VALUES ('Van', 300, 12, 0, 2, 0)");

    
    for(int i = 0; i < 120; i++){
      int cod = codmes+1;
      database.execute("INSERT INTO mes(nome_mes, num_mes, ano) VALUES ('${meses[codmes]}', '$cod', $ano)");
      if(codmes == 11){
        codmes = 0;
        ano++;
      }else{
        codmes++;
      }

    }
    await database.execute("INSERT INTO gastos_mes(mes_id_mes, gastos_id_gastos) VALUES (1, 1)");
    await database.execute("INSERT INTO gastos_mes(mes_id_mes, gastos_id_gastos) VALUES (1, 2)");
    await database.execute("INSERT INTO gastos_mes(mes_id_mes, gastos_id_gastos) VALUES (1, 3)");
    await database.execute("INSERT INTO gastos_mes(mes_id_mes, gastos_id_gastos) VALUES (2, 2)");
  
    */
    
    //await database.execute("INSERT INTO desejos(nome_desejos, valor_desejos, prioridade) VALUES ('Faculdade', 120, 1)");
    
    var gastos = await database.query("gastos",
    columns: ["id_gastos", "nome", "valor", "qtd_parcelas", "fixo"],
    where: "id_gastos>?",
    whereArgs: ["0"]);
    

    for (var item in gastos){
      setState(() {
       _lista += item['nome']+"\n"; 
      });
    }

    var mes = await database.query("mes",
    columns: ["id_mes", "num_mes", "nome_mes", "ano"],
    where: "id_mes>?",
    whereArgs: ["0"]);
    

    for (var item in mes){
      setState(() {


       _lista += item['nome_mes']+"\n"; 
      });
    }

    var desejos = await database.query("desejos",
    columns: ["id_desejos", "nome_desejos", "valor_desejos", "prioridade"],
    where: "id_desejos>?",
    whereArgs: ["0"]);
    

    for (var item in desejos){
      setState(() {
       _lista += item['nome_desejos']+"\n"; 
      });
    }


    var gastosmes = await database.rawQuery('SELECT * FROM gastos_mes');

    lista2 = gastosmes.toList();

    //var inner = await database.rawQuery('SELECT * FROM mes INNER JOIN gastos ON mes.id_mes = id_mes');
    var inner = await database.rawQuery('SELECT * FROM gastos_mes INNER JOIN mes ON mes.id_mes = mes_id_mes INNER JOIN gastos ON gastos.id_gastos = gastos_id_gastos');
    //seleciona todos os registros
    lista3 = inner.toList();

    var inner2 = await database.rawQuery('SELECT * FROM gastos_mes INNER JOIN mes ON mes.id_mes = mes_id_mes INNER JOIN gastos ON gastos.id_gastos = gastos_id_gastos WHERE nome_mes = "Janeiro" AND ano = 2019');
    //seleciona só os do mes que quiser
    lista4 = inner2.toList();

    /*for (var item in lista4){
      setState(() {
       _lista += item["nome"].toString()+"\n"; 
        _lista += item["valor"].toString()+"\n"; 
        _lista += item["nome_mes"].toString()+"\n"; 
        _lista += item["ano"].toString()+"\n"; 
      });
    }*/



    /*await database.rawDelete('delete from gastos');
    await database.rawDelete('delete from mes');*/
    await database.close();

    debugPrint(_lista);
    debugPrint(lista2.toString());
    debugPrint(lista3.toString());
    debugPrint(lista4.toString());
    
    debugPrint(new DateFormat("MM").format(now));
  }
  var now = new DateTime.now();
  String _lista = "";
  List lista2;
  List lista3;
  List lista4;
  
  /*Map<dynamic, dynamic> MeusMeses = {
      "mes":[
          {
            "id":"",
            "num_mes":"",
            "nome":"",
            "ano":"",
          },
      ]
  };
*/


  @override
  void initState() {
    super.initState();

    _executar();

   /* CREATE TABLE IF NOT EXISTS `mydb`.`Gastos` (
  `idGastos` INT NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(45) NULL,
  `valor` DOUBLE NULL,
  `qtdParcelas` INT NULL,
  `fixo` INT NULL DEFAULT 0,
  PRIMARY KEY (`idGastos`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Meses`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Meses` (
  `idMeses` INT NOT NULL AUTO_INCREMENT,
  `nome` INT NOT NULL,
  `ano` INT NOT NULL,
  PRIMARY KEY (`idMeses`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Gastos_Meses`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Gastos_Meses` (
  `Gastos_idGastos` INT NOT NULL,
  `Meses_idMeses` INT NOT NULL,
  PRIMARY KEY (`Gastos_idGastos`, `Meses_idMeses`),
  INDEX `fk_Gastos_has_Meses_Meses1_idx` (`Meses_idMeses` ASC) VISIBLE,
  INDEX `fk_Gastos_has_Meses_Gastos_idx` (`Gastos_idGastos` ASC) VISIBLE,
  CONSTRAINT `fk_Gastos_has_Meses_Gastos`
    FOREIGN KEY (`Gastos_idGastos`)
    REFERENCES `mydb`.`Gastos` (`idGastos`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Gastos_has_Meses_Meses1`
    FOREIGN KEY (`Meses_idMeses`)
    REFERENCES `mydb`.`Meses` (`idMeses`)
    */
    Timer(
        Duration(seconds: 3),
        () => Navigator.of(context).pushReplacement(MaterialPageRoute(
            builder: (BuildContext context) => TelaInicial()))
            );

  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          Container(
            decoration: BoxDecoration(
              //color: new Color(0xff622f74),
              gradient: LinearGradient(
                colors: [new Color.fromRGBO(232, 100, 130, 1.0), new Color.fromRGBO(	232, 100, 0, 1.0)],
                begin: Alignment.topRight,
                end: Alignment.bottomLeft,
                //colors: [Colors.blue, Colors.red],
                ),
            ),
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,

            children: <Widget>[
              CircleAvatar(
                backgroundColor: Colors.white,
                radius: 50.0,
                child: Icon(
                  Icons.monetization_on,
                  color: Colors.deepOrange,
                  size: 50.0,
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 10.0),
              ),
              Text(
                'MEUS GASTOS',
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 24.0
                  ),
                ),
            ],
          ),
        ],
      ),
    );
  }
}