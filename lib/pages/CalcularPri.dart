import 'package:flutter/material.dart';

class CalcularPri extends StatefulWidget {
  @override
  _State createState() => _State();
}

class _State extends State<CalcularPri> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.blue[200],
      appBar: new AppBar(
          title: new Text('Calcular Gastos'),
          ),
      body: Padding(
        padding: EdgeInsets.all(10),
        child:Center(
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              TextFormField(
                autofocus: true,
                keyboardType: TextInputType.text,
                style: new TextStyle(color: Colors.white, fontSize: 20),
                decoration: InputDecoration(
                  labelText: "Digite um texto",
                  labelStyle: TextStyle(color: Colors.white),
                ),
              ),
              Divider(),
              TextFormField(
                autofocus: true,
                keyboardType: TextInputType.number,
                style: new TextStyle(color: Colors.white, fontSize: 20),
                decoration: InputDecoration(
                  labelText: "Digite um número",
                  labelStyle: TextStyle(color: Colors.white),
                ),
              ),
              Divider(),
              ButtonTheme(
                height: 60.0,
                child: RaisedButton(
                  onPressed: () => {},
                  child: Text(
                  "Calcular", style: TextStyle(color: Colors.blue[200]),
                  ),
                  color: Colors.white,
                ),
              ),

        ],
      ),
      )
      )
    );
  }
}