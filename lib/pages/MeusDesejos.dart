import 'package:flutter/material.dart';
import 'package:flutter_app/models/DialogAdicionarDesejos.dart';
import 'package:flutter_app/pages/SplashScreen.dart';
import 'package:flutter_app/pages/TelaInicial.dart';
import 'package:path_provider/path_provider.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';
import 'dart:async';
import 'package:intl/intl.dart';
import 'dart:io';
import 'package:gradient_app_bar/gradient_app_bar.dart';

import 'package:toast/toast.dart';
import '../models/MeusWidgets.dart';

class MeusDesejos extends StatefulWidget {
  @override
  _MeusDesejosState createState() => _MeusDesejosState();
}

class _MeusDesejosState extends State<MeusDesejos> {

  final List<String> meses = <String>[
    'Janeiro',
    'Fevereiro',
    'Março',
    'Abril',
    'Maio',
    'Junho',
    'Julho',
    'Agosto',
    'Setembro',
    'Outubro',
    'Novembro',
    'Dezembro'
  ];

  double receitas = 0, despesas = 0, saldo = 0;
  String rec, desp, sald, nomeMaiusculo, nome, test = "Van", nomeCompra, valorEstimado;
  double alturaDaTela, larguraDaTela;
  int _radioValue = 2, navValor=1;
  int _radioValue2 = 1, valorPrioridade;
  double tamDig = 0.40;
  
      final nomeController = TextEditingController();
      final valorController = TextEditingController();
      final valorPagoController = TextEditingController();

static var now = new DateTime.now();
      int anoAtual = int.parse(new DateFormat("yyyy").format(now));
  int mesAtual = int.parse(new DateFormat("MM").format(now)) - 1;

  
  final List<String> gastos = <String>[];
  final List<double> valor = <double>[];
  final List<int> prioridade = <int>[];

  List listaDesejos1;
  List listaDesejos2;
  List listaDesejos3;

  var isSelected3 = [true, false];
  int tipo = 1, repete = 2;
  int tipo_fixo = 0;
  bool parcelado = false;
  //final valor = TextEditingController();
  final num_Parcelas = TextEditingController();


  var isSelected2 = [true, false];

  bool todos;
     int gastoAtual = 0,
      cont = 1;
      @override
  void initState() {
    super.initState();

    Consultar();
  }

  @override
  Widget build(BuildContext context) {
    alturaDaTela = MediaQuery.of(context).size.height;
    larguraDaTela = MediaQuery.of(context).size.width;
    rec = receitas.toStringAsFixed(2);
    desp = despesas.toStringAsFixed(2);
    sald = saldo.toStringAsFixed(2);
    return Scaffold(
      appBar: GradientAppBar(
    title: Text('Meus Desejos'),
    backgroundColorStart: Color.fromRGBO(224, 49, 40, 1.0),
    backgroundColorEnd: Color.fromRGBO(224, 49, 40, 0.75),
    
    actions: <Widget>[
            // action button
            IconButton(
              icon: Icon(Icons.add),
              color: Colors.white,
              onPressed: AdicionarDesejos,
            ),
          ]),
  
      
      /* AppBar(
          title: Text('Meus Gastos'),
          backgroundColor: Color.fromRGBO(224, 49, 40, 1.0),
          actions: <Widget>[
            // action button
            IconButton(
              icon: Icon(IconData(57669, fontFamily: 'MaterialIcons')),
              onPressed: Adicionar,
            ),
          ]),*/
      body: Stack(fit: StackFit.expand, children: <Widget>[
        Container(
          decoration: BoxDecoration(
            color: new Color(0xff622f74),
            gradient: LinearGradient(
              colors: [
                new Color.fromRGBO(232, 100, 106, 1.0),
                new Color.fromRGBO(232, 100, 0, 1.0)
              ],
              begin: Alignment.centerRight,
              end: Alignment.centerLeft,
            ),
          ),
        ),
        Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          //crossAxisAlignment: CrossAxisAlignment.baseline,
          children: <Widget>[
            Expanded(
              child: ConteudoMes(),
            ),
            Padding(
              padding: EdgeInsets.only(top: 1.0),
            ),
            NavValores(),
          ],
        ),
      ]),
    );
  }

  Widget ConteudoMes() {
    return Container(
      //color: Color.fromRGBO(224, 49, 40, 1.5),
      decoration: BoxDecoration(
        gradient: LinearGradient(
                  colors: [
                    new Color.fromRGBO(232, 100, 106, 1.0),
                    new Color.fromRGBO(232, 100, 0, 1.0)
                  ],
                  begin: Alignment.centerRight,
                  end: Alignment.centerLeft,
                ),
      ),
      height: alturaDaTela * 0.64,
      child: CustomScrollView(
        shrinkWrap: true,
        slivers: <Widget>[
          new SliverList(
            delegate: new SliverChildBuilderDelegate(
              (context, gastoAtual) => new Card(
                child: new Container(
                  /*decoration: new BoxDecoration(
                  gradient: LinearGradient(
                  colors: [
                    new Color.fromRGBO(33, 222, 72, 0.0),
                    new Color.fromRGBO(222, 33, 42, 0.6),
                    //new Color.fromRGBO(33, 222, 72, 0.4),
                  ],
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  stops: [
                  0.85,
                  0.99
                ],
                ),
                  ),*/
                  padding: EdgeInsets.all(10.0),
                  child: new Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      IconPersonalizado(prioridade[gastoAtual]),
                      Padding(
                        padding: EdgeInsets.all(10),
                      ),
                      Expanded(
                        child: Text(
                          ' ${gastos[gastoAtual]} \n RS: ${valor[gastoAtual].toStringAsFixed(2)}',
                          textAlign: TextAlign.left,
                          style: TextStyle(
                            color: Colors.black,
                            fontSize: 20.0,
                          ),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.all(10),
                      ),
                      IconButton(
                        alignment: Alignment.centerRight,
                        icon: Icon(Icons.monetization_on),
                        tooltip: 'Comprar',
                        onPressed: () {
                          setState(() {
                            nome = gastos[gastoAtual];
                            tipo = prioridade[gastoAtual];
                            valorEstimado = valor[gastoAtual].toStringAsFixed(2);
                            Comprar(gastos[gastoAtual]);
                          });
                        },
                      ),
                      IconButton(
                        alignment: Alignment.centerRight,
                        icon: Icon(Icons.create),
                        tooltip: 'Editar',
                        onPressed: () {
                          setState(() {
                            nome = gastos[gastoAtual];
                            tipo = prioridade[gastoAtual];
                            Editar(gastos[gastoAtual], valor[gastoAtual]);
                          });
                        },
                      ),
                      IconButton(
                        alignment: Alignment.centerRight,
                        icon: Icon(Icons.delete),
                        tooltip: 'Excluir',
                        onPressed: () {
                          setState(() {
                            nome = gastos[gastoAtual];
                            Excluir(gastos[gastoAtual]);
                            //
                          });
                        },
                      ),
                    ],
                  ),
                ),
              ),
              childCount: gastos.length,
            ),
          ),
        ],
      ),
    );
  }
  Widget IconPersonalizado(gasto) {
    if (gasto == 1) {
      return CircleAvatar(
        backgroundColor: Colors.green,
        child: Icon(
          Icons.attach_money,
          color: Colors.white,
        ),
      );
    } else if (gasto == 2) {
      return CircleAvatar(
        backgroundColor: Colors.deepOrange,
        child: Icon(
          Icons.attach_money ,
          color: Colors.white,
        ),
      );
    }
    else if (gasto == 3) {
      return CircleAvatar(
        backgroundColor: Colors.redAccent[700],
        child: Icon(
          Icons.attach_money ,
          color: Colors.white,
        ),
      );
    }
  }
  Widget NavValores(){
    return Container(
              padding: EdgeInsets.all(3.0),
              decoration: BoxDecoration(
                
              color: Color.fromRGBO(224, 49, 40, 1.5),
                border: Border(top: BorderSide(width: 1.0, color: Colors.amber[900]),),
              ),
              child: Stack(
                children: <Widget>[
                  Column(
                    children: <Widget>[
                      Saldo(),
                    ],
                  ),
                ],
              ),
            );
    }
  

  Widget Saldo() {
    return Container(
      //Mes e botões
      color: Color.fromRGBO(224, 49, 40, 1.5),
      child: Row(
          mainAxisAlignment: MainAxisAlignment.end,
          mainAxisSize: MainAxisSize.max,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              'Saldo:',
              style: TextStyle(color: Colors.white, fontSize: 22.0),
            ),
            RetornaSaldo(),
          ]),
    );
  }
  Widget RetornaSaldo(){
    if(saldo >= 0){
      return Text(
              '  RS: $sald',
              style: TextStyle(color: Colors.greenAccent, fontSize: 22.0),
            );
    }else{
      return Text(
              '  RS: $sald',
              style: TextStyle(color: Colors.red[100], fontSize: 22.0),
            );
    }
  }
  void AdicionarDesejos() {
    Toast.show("Clicou no botão adicionar", context,
        duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
    showDialog(
      context: context,
      builder: (_) => DialogAdicionarDesejos(),
    );
  }
  void _handleRadioValueChange2(int value) {
    //debugPrint("Chegou");
    setState(() {
      _radioValue2 = value;

      switch (_radioValue) {
        case 1:
          parcelado = false;
          tipo_fixo = 1;
          //Comprar(nomeMaiusculo);
          //Toast.show("Não repete", context, duration: Toast.LENGTH_SHORT, gravity:  Toast.BOTTOM);
          break;
        case 2:
          parcelado = true;
          tipo_fixo = 2;
          //Comprar(nomeMaiusculo);
          //Toast.show("Repete", context, duration: Toast.LENGTH_SHORT, gravity:  Toast.BOTTOM);
          break;
      }
      //debugPrint("Radio value dps do switch"+ _radioValue.toString());
    });
  }
  void _handleRadioValueChange(int value) {
    setState(() {
      _radioValue = value;

      switch (_radioValue) {
        case 1:
          valorPrioridade = 1;
          //Toast.show("Não repete", context, duration: Toast.LENGTH_SHORT, gravity:  Toast.BOTTOM);
          break;
        case 2:
          valorPrioridade = 2;
          //Toast.show("Repete", context, duration: Toast.LENGTH_SHORT, gravity:  Toast.BOTTOM);
          break;
        case 3:
          valorPrioridade = 3;
          //Toast.show("Repete", context, duration: Toast.LENGTH_SHORT, gravity:  Toast.BOTTOM);
          break;
      }
    });
  }

  Widget Excluir(String qualGasto) {
    //Toast.show("Clicou no botão excluir: " + qualGasto, context,
    //duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
    nomeMaiusculo = qualGasto.toUpperCase();
    //nome = qualGasto;

    showDialog(
      context: context,
      //builder: (_) => DialogExcluir(),
      builder: (BuildContext context) {
        return Center(
          child: Material(
            color: Colors.transparent,
            child: Container(
                margin: EdgeInsets.all(20.0),
                padding: EdgeInsets.all(15.0),
                height: alturaDaTela * 0.25,
                decoration: ShapeDecoration(
                  gradient: LinearGradient(
                  colors: [
                    new Color.fromRGBO(232, 100, 106, 1.0),
                    new Color.fromRGBO(232, 100, 0, 1.0)
                  ],
                  begin: Alignment.centerRight,
                  end: Alignment.centerLeft,
                ),
                  
                  shape: RoundedRectangleBorder(
                    //side: BorderSide.lerp(a, b, t),
                      borderRadius: BorderRadius.circular(15.0)),
                      
                ),
                child: Column(
                  children: <Widget>[
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.only(
                            top: 10.0, left: 20.0, right: 20.0),
                        child: Text(
                          "$nomeMaiusculo",
                          style: TextStyle(color: Colors.white, fontSize: 20.0),
                        ),
                      ),
                    ),
                    Text(
                      "Deseja realmente excluir ?",
                      style: TextStyle(color: Colors.white, fontSize: 18.0),
                    ),
                    BotoesFinais(1),
                  ],
                )),
          ),
        );
      },
    );
  }
  Widget Comprar(String qualGasto) {
    //Toast.show("Clicou no botão excluir: " + qualGasto, context,
    //duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
    nomeCompra = qualGasto;
    nomeMaiusculo = qualGasto.toUpperCase();
    setState(() {
      valorPagoController.text = valorEstimado;
    });
    

    
    //nome = qualGasto;
    
    showDialog(
      context: context,
      //builder: (_) => DialogExcluir(),
      builder: (BuildContext context) {
        return Center(
          child: Material(
            color: Colors.transparent,
            child: Container(
                margin: EdgeInsets.all(20.0),
                padding: EdgeInsets.all(15.0),
                height: alturaDaTela * tamDig,
                decoration: ShapeDecoration(
                  gradient: LinearGradient(
                  colors: [
                    new Color.fromRGBO(232, 100, 106, 1.0),
                    new Color.fromRGBO(232, 100, 0, 1.0)
                  ],
                  begin: Alignment.centerRight,
                  end: Alignment.centerLeft,
                ),
                  
                  shape: RoundedRectangleBorder(
                    //side: BorderSide.lerp(a, b, t),
                      borderRadius: BorderRadius.circular(15.0)),
                      
                ),
                child: Column(
                  children: <Widget>[
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.only(
                            top: 10.0, left: 20.0, right: 20.0),
                        child: Text(
                          "$nomeMaiusculo",
                          style: TextStyle(color: Colors.white, fontSize: 20.0),
                        ),
                      ),
                    ),
                            TextFormField(
                              autofocus: false,
                              keyboardType: TextInputType.number,
                              style: new TextStyle(
                                  color: Colors.white, fontSize: 20),
                              decoration: InputDecoration(
                                labelText: "Valor Pago:",
                                labelStyle: TextStyle(color: Colors.white),
                              ),
                              controller: valorPagoController,
                            ),
                            SizedBox(height: 10),
                    ToggleButtons(
              hoverColor: Colors.green,
              fillColor: Colors.green[800],
              color: Colors.red[900],
              selectedColor: Colors.red[900],
              borderColor: Colors.green[900],
              selectedBorderColor: Colors.green[900],
              focusColor: Colors.green[100],
              splashColor: Colors.green[100],
              disabledColor: Colors.green[900],
              borderWidth: 3,
              borderRadius: BorderRadius.circular(15.0),
              children: [
                Text(
                  ' NÃO REPETE ',
                  style: TextStyle(color: Colors.white),
                ),
                Text(
                  ' REPETE ',
                  style: TextStyle(color: Colors.white),
                ),
              ],
              onPressed: (int index) {
                setState(() {
                  for (int buttonIndex = 0;
                      buttonIndex < isSelected3.length;
                      buttonIndex++) {
                    if (buttonIndex == index) {
                      isSelected3[buttonIndex] = true;
                      repete = 1;
                      
                      _radioValue2 = 1;
                      tipo_fixo = 1;
                      tamDig = 0.50;
                    } else {
                      isSelected3[buttonIndex] = false;
                      repete = 2;
                      tamDig = 0.40;
                    }
                  }
                  
                                  Navigator.of(context).pop();
                                  Comprar(qualGasto);
                                
                });
              },
              isSelected: isSelected3,
            ),
            Repete(),
            Parcelado(),
                    BotoesFinais(3),
                  ],
                )),
          ),
        );
      },
    );
  }
  Widget Repete() {
    if (isSelected3[1] == true) {
      return Align(
        alignment: Alignment.topCenter,
        child: Container(
          child: Stack(children: <Widget>[
            Column(children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Radio(
                    value: 1,
                    groupValue: _radioValue2,
                    onChanged: (val) {
                            setState(() {
                              Navigator.of(context).pop();
                              _handleRadioValueChange2(1);
                              parcelado = false;
                              tipo_fixo = 1;
                              tamDig = 0.50;
                              Comprar(nomeMaiusculo);
                            });
                          },
                  ),
                  Text(
                    'Fixo',
                    style: TextStyle(color: Colors.white),
                  ),
                  new Radio(
                    value: 2,
                    groupValue: _radioValue2,
                    onChanged: (val) {
                            setState(() {
                              Navigator.of(context).pop();
                              _handleRadioValueChange2(2);
                              parcelado = true;
                              tipo_fixo = 2;
                              tamDig = 0.60;
                              Comprar(nomeMaiusculo);
                              
                            });
                          },
                  ),
                  new Text(
                    'Parcelado',
                    style: TextStyle(color: Colors.white),
                  ),
                ],
              ),
            ]),
          ]),
        ),
      );
    } else {
      parcelado = false;
      _radioValue = 0;
      _radioValue2 = 0;
      tipo_fixo = 0;
      num_Parcelas.text = "0";
      return SizedBox(height: 10);
    }
  }

  Widget Parcelado() {
    if (parcelado == true) {
      return Align(
        alignment: Alignment.topCenter,
        child: Container(
          child: Stack(children: <Widget>[
            Column(children: <Widget>[
              TextFormField(
                autofocus: false,
                keyboardType: TextInputType.number,
                style: new TextStyle(color: Colors.white, fontSize: 20),
                decoration: InputDecoration(
                  labelText: "Numero de Parcelas:",
                  labelStyle: TextStyle(color: Colors.white),
                ),
                controller: num_Parcelas,
              ),
            ]),
          ]),
        ),
      );
    } else {
      num_Parcelas.text = "0";
      return SizedBox(height: 10);
    }
  }


  Widget BotoesFinais(int condicao) {
    return Expanded(
        child: Row(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.end,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.all(10.0),
          child: ButtonTheme(
              height: 35.0,
              minWidth: 110.0,
              child: RaisedButton(
                color: Colors.white,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(5.0)),
                splashColor: Colors.white.withAlpha(40),
                child: Text(
                  'Voltar',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      color: Colors.orange[900],
                      fontWeight: FontWeight.bold,
                      fontSize: 13.0),
                ),
                onPressed: () {
                  Navigator.of(context).pop();
                  /*setState(() {
                                      Route route = MaterialPageRoute(
                                          builder: (context) => LoginScreen());
                                      Navigator.pushReplacement(context, route);
                                    });*/
                },
              )),
        ),
        Padding(
            padding: const EdgeInsets.only(
                left: 20.0, right: 10.0, top: 10.0, bottom: 10.0),
            child: ButtonTheme(
                height: 35.0,
                minWidth: 110.0,
                child: RaisedButton(
                  color: Colors.white,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(5.0)),
                  splashColor: Colors.white.withAlpha(40),
                  child: Text(
                    'Confirmar',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        color: Colors.orange[900],
                        fontWeight: FontWeight.bold,
                        fontSize: 13.0),
                  ),
                  onPressed: () {
                    setState(() {
                      //excluir
                      if (condicao == 1) {
                        // debugPrint("Excluir e Todos :" + todos.toString());
                        ExecutarExclusao(nome);
                      }
                      //editar
                      else if (condicao == 2) {
                        validaCampos(nome);
                      }
                      else if (condicao == 3) {
                        validaCampos2();
                      }
                      /* Route route = MaterialPageRoute(
                                          builder: (context) => LoginScreen());
                                      Navigator.pushReplacement(context, route);
                                   */
                    });
                  },
                ))),
      ],
    ));
  }

  void Editar(String qualGasto, double valorGasto) {
    setState(() {
      int valorInt = valorGasto.toInt();
     valorController.text=valorInt.toString(); 
     nomeController.text=qualGasto.toString(); 
     if (tipo == 1) {
        isSelected2[0] = true;
        isSelected2[1] = false; 
     } else if(tipo == 2) {
        isSelected2[0] = false;
        isSelected2[1] = true; 
      }
     //tipo = tipGasto;
    });
    //debugPrint(tipGasto.toString()+"tipo:"+tipo.toString());
    showDialog(
      context: context,
      //builder: (_) => DialogExcluir(),
      builder: (BuildContext context) {
        return Center(
          child: Material(
            color: Colors.transparent,
            child: Container(
                margin: EdgeInsets.all(20.0),
                padding: EdgeInsets.all(10.0),
                height: alturaDaTela * 0.50,
                decoration: ShapeDecoration(
                  gradient: LinearGradient(
                  colors: [
                    new Color.fromRGBO(232, 100, 106, 1.0),
                    new Color.fromRGBO(232, 100, 0, 1.0)
                  ],
                  begin: Alignment.centerRight,
                  end: Alignment.centerLeft,
                ),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(15.0)),
                ),
                child: Column(
                  children: <Widget>[
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.only(
                            top: 10.0, left: 20.0, right: 20.0),
                        child: Text(
                          "EDITAR",
                          style: TextStyle(color: Colors.white, fontSize: 20.0),
                        ),
                      ),
                    ),
                    Align(
                      alignment: Alignment.topCenter,
                      child: Container(
                        child: Stack(children: <Widget>[
                          Column(children: <Widget>[
                            TextFormField(
                              autofocus: false,
                              keyboardType: TextInputType.text,
                              style: new TextStyle(
                                  color: Colors.white, fontSize: 20),
                              decoration: InputDecoration(
                                labelText: "Nome:",
                                labelStyle: TextStyle(color: Colors.white),
                              ),
                              controller: nomeController,
                            ),
                            Divider(),
                            TextFormField(
                              autofocus: false,
                              keyboardType: TextInputType.number,
                              style: new TextStyle(
                                  color: Colors.white, fontSize: 20),
                              decoration: InputDecoration(
                                labelText: "Valor:",
                                labelStyle: TextStyle(color: Colors.white),
                              ),
                              controller: valorController,
                            ),
                            SizedBox(height: 10),

              Text(
                    'PRIORIDADE',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                        fontSize: 13.0),
                  ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Radio(
                    value: 1,
                    groupValue: _radioValue,
                    onChanged: (val) {
                            setState(() {
                              Navigator.of(context).pop();
                              _handleRadioValueChange(1);
                              Editar(qualGasto, valorGasto);
                            });
                          },
                  ),
                  Text(
                    'Baixo',
                    style: TextStyle(color: Colors.white),
                  ),
                  new Radio(
                    value: 2,
                    groupValue: _radioValue,
                    onChanged: (val) {
                            setState(() {
                              Navigator.of(context).pop();
                              _handleRadioValueChange(2);
                              Editar(qualGasto, valorGasto);
                            });
                          },
                  ),
                  new Text(
                    'Medio',
                    style: TextStyle(color: Colors.white),
                  ),
                  new Radio(
                    value: 3,
                    groupValue: _radioValue,
                    onChanged: (val) {
                            setState(() {
                              Navigator.of(context).pop();
                              _handleRadioValueChange(3);
                              Editar(qualGasto, valorGasto);
                            });
                          },
                  ),
                  new Text(
                    'Alto',
                    style: TextStyle(color: Colors.white),
                  ),
                ],
              ),
                          ]),
                        ]),
                      ),
                    ),
                    BotoesFinais(2),
                  ],
                )),
          ),
        );
      },
    );
  }
  retornaCorFill(int tipo){
        if(tipo == 1){
          return Colors.green[800];
        }else{
          return Colors.red[800];
        }
      }
      retornaCorBorder(int tipo){
        if(tipo == 1){
          return Colors.red[800];
        }else{
          return Colors.green[800];
        }
      }
      retornaCorBorderSelected(int tipo){
        if(tipo == 1){
          return Colors.green[900];
        }else{
          return Colors.red[900];
        }
      }

  void Consultar() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = documentsDirectory.path + "/GastosDB.db";
    var database = await openDatabase(path, version: 3);

    var inner1 = await database.rawQuery(
        'SELECT * FROM desejos WHERE prioridade = 1');
    var inner2 = await database.rawQuery(
        'SELECT * FROM desejos WHERE prioridade = 2');
    var inner3 = await database.rawQuery(
        'SELECT * FROM desejos WHERE prioridade = 3');
    //seleciona só os do mes que quiser
    listaDesejos1 = inner1.toList();
    listaDesejos2 = inner2.toList();
    listaDesejos3 = inner3.toList();

    //mesList.add("teste");
    setState(() {
      gastos.clear();
      valor.clear();
      prioridade.clear();
    });

    saldo = 0;

    for (var item in listaDesejos3) {
      setState(() {
        gastos.add(item["nome_desejos"].toString());
        valor.add(item["valor_desejos"]);
        prioridade.add(item["prioridade"]);
        saldo += item["valor_desejos"];
      });
    }

    for (var item in listaDesejos2) {
      setState(() {
        gastos.add(item["nome_desejos"].toString());
        valor.add(item["valor_desejos"]);
        prioridade.add(item["prioridade"]);
        saldo += item["valor_desejos"];
      });
    }

    for (var item in listaDesejos1) {
      setState(() {
        gastos.add(item["nome_desejos"].toString());
        valor.add(item["valor_desejos"]);
        prioridade.add(item["prioridade"]);
        saldo += item["valor_desejos"];
      });
    }

    await database.close();

    setState(() {
      sald = saldo.toStringAsFixed(2);
    });

    debugPrint(listaDesejos1.toString());
  }

   void validaCampos(String name){
     if (valorController.text.contains(",")) {
      setState(() {
        valorController.text = valorController.text.replaceAll(new RegExp(r","), ".");
      });
    }
    if(nomeController.text.length <2){
      Toast.show("Nome inválido", context,
        duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
    }
    else if(valorController.text.length < 1 || double.parse(valorController.text) < 1){
          Toast.show("Valor inválido", context,
          duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
    }else if(valorPrioridade <1 || valorPrioridade >3){
      Toast.show("Informe um nível de prioridade", context,
          duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
    }
    
    else{
          ExecutarEdicao(name, nomeController.text, double.parse(valorController.text));
        }
  }

  void ExecutarExclusao(String name) async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = documentsDirectory.path + "/GastosDB.db";
    var database = await openDatabase(path, version: 3);

    var desejo = await database
        .rawQuery("SELECT id_desejos FROM desejos WHERE nome_desejos =" + "'$name'");
    //seleciona só os do mes que quiser
    int idDesejo;

    List meuDesejo = desejo.toList();

    for (var item in meuDesejo) {
      setState(() {
        idDesejo = item["id_desejos"];
      });
    }
    

    
      
      await database.execute(
          " DELETE FROM desejos WHERE id_desejos = " + idDesejo.toString());
    

    await database.close();

    await Consultar();

    Navigator.of(context).pop();
  }
  void ExecutarEdicao(String name, String nvNome, double nvValor) async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = documentsDirectory.path + "/GastosDB.db";
    var database = await openDatabase(path, version: 3);

    var gasto = await database
        .rawQuery("SELECT id_desejos FROM desejos WHERE nome_desejos =" + "'$name'");
    
    //seleciona só os do mes que quiser
    int idGasto;

    List meuGasto = gasto.toList();

    for (var item in meuGasto) {
      setState(() {
        idGasto = item["id_desejos"];
      });
    }
    debugPrint(idGasto.toString());
    
    await database.execute("UPDATE desejos SET prioridade = "+valorPrioridade.toString()+", nome_desejos = '"+nvNome+"', valor_desejos = "+nvValor.toString()+" WHERE id_desejos = "+idGasto.toString());
    

    await database.close();

    await Consultar();

    Navigator.of(context).pop();
  }
  void validaCampos2() {
    if (valorPagoController.text.contains(",")) {
      setState(() {
        valorPagoController.text = valorPagoController.text.replaceAll(new RegExp(r","), ".");
      });
    } else if (valorPagoController.text.length < 1 || double.parse(valorPagoController.text) < 1) {
      Toast.show("Valor inválido", context,
          duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
    } else {
      Cadastrar();
    }
  }

  void Cadastrar() async {
    //Toast.show("Tipo: "+ tipo.toString() +"  Nome: " + nome.text+ "  Valor: "+valor.text+ "\n  Repete: "+repete.toString()+"  Parcelado:"+tipo_fixo.toString()+" Numero de Parcelas: "+num_Parcelas.text, context, duration: Toast.LENGTH_LONG, gravity:  Toast.BOTTOM);
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = documentsDirectory.path + "/GastosDB.db";
    var database = await openDatabase(path, version: 2);

    debugPrint("TIPO FIXO : "+tipo_fixo.toString());

    //não repete
    if (repete == 2 && tipo_fixo == 0) {
      await database.execute( "INSERT INTO gastos(nome, valor, qtd_parcelas, parcelas_pagas, tipo, fixo) VALUES ('"+nomeCompra+"', "+valorPagoController.text+", 1, 0, 2, 0)");
    debugPrint("chegou");
    } else if (repete == 1 && tipo_fixo == 1) {
      await database.execute(
          "INSERT INTO gastos(nome, valor, qtd_parcelas, parcelas_pagas, tipo, fixo) VALUES ('" +
              nomeCompra +
              "', " +
              valorPagoController.text +
              ", 0, 0, 2" +
              ", 1)");
    } else if (repete == 1 && tipo_fixo == 2) {
      double ValorParcelas =
          double.parse(valorPagoController.text) / int.parse(num_Parcelas.text);
      await database.execute(
          "INSERT INTO gastos(nome, valor, qtd_parcelas, parcelas_pagas, tipo, fixo) VALUES ('" +
              nomeCompra +
              "', " +
              ValorParcelas.toString() +
              ", " +
              num_Parcelas.text +
              ", 0, 2" +
              ", 0)");
    }

    var gasto = await database.rawQuery(
        "SELECT id_gastos FROM gastos WHERE nome = '" + nomeCompra + "'");
    String teste =
        "SELECT id_gastos FROM gastos WHERE nome = '" + nomeCompra + "'";
    debugPrint(teste);

    var ultimo_Mes = await database.rawQuery(
        "SELECT * FROM mes WHERE id_mes = (SELECT MAX(id_mes) FROM mes)");

    debugPrint(anoAtual.toString());

    var atual_Mes = await database.rawQuery(
        "SELECT * FROM mes WHERE nome_mes = '" +
            meses[mesAtual] +
            "' AND ano = " +
            anoAtual.toString() +
            "");

    //var gasto = await database.rawQuery("SELECT id_gastos FROM gastos WHERE nome = '"+nome.text+"'");

    int idGasto;

    List meuGasto = gasto.toList();

    for (var item in meuGasto) {
      setState(() {
        idGasto = item["id_gastos"];
      });
    }

    int idMes;

    List meuMes = ultimo_Mes.toList();

    for (var item in meuMes) {
      setState(() {
        idMes = item["id_mes"];
      });
    }

    int atualMes;

    List meuMesAtual = atual_Mes.toList();

    for (var item in meuMesAtual) {
      setState(() {
        atualMes = item["id_mes"];
      });
    }

    int nParcelas = int.parse(num_Parcelas.text);

    //não repete
    if (repete == 2) {
      await database.execute(
          "INSERT INTO gastos_mes(mes_id_mes, gastos_id_gastos) VALUES (" +
              atualMes.toString() +
              ", " +
              idGasto.toString() +
              ")");
    } else if (repete == 1 && tipo_fixo == 1) {
      for (int i = 1; i <= idMes; i++) {
        //debugPrint(i.toString());
        await database.execute(
            "INSERT INTO gastos_mes(mes_id_mes, gastos_id_gastos) VALUES (" +
                i.toString() +
                ", " +
                idGasto.toString() +
                ")");
      }
    } else if (repete == 1 && tipo_fixo == 2) {
      for (int i = 1; i <= nParcelas; i++) {
        int rep = atualMes + i - 1;
        //debugPrint(rep.toString());
        await database.execute(
            "INSERT INTO gastos_mes(mes_id_mes, gastos_id_gastos) VALUES (" +
                rep.toString() +
                ", " +
                idGasto.toString() +
                ")");
      }
    }
    database.close();

    debugPrint("Repete:" + repete.toString());
    debugPrint("Tipo Fixo:" + tipo_fixo.toString());
    debugPrint("ID Gasto:" + idGasto.toString());
    debugPrint(meuMes.toString());
    debugPrint("ID Mes:" + idMes.toString());

    //var inner2 = await database.rawQuery('SELECT * FROM gastos_mes INNER JOIN mes ON mes.id_mes = mes_id_mes INNER JOIN gastos ON gastos.id_gastos = gastos_id_gastos WHERE nome_mes = "${meses[mesAtual]}" AND ano = 20$ano');
    //seleciona só os do mes que quiser

    debugPrint("Tipo: " +
        tipo.toString() +
        "  Nome: " +
        nomeCompra +
        "  Valor: " +
        valorPagoController.text +
        "\n  Repete: " +
        repete.toString() +
        "  Parcelado:" +
        tipo_fixo.toString() +
        " Numero de Parcelas: " +
        num_Parcelas.text);
    
    Navigator.of(context).pop();
    
    Navigator.of(context).pop();
    ExecutarExclusao(nomeCompra);

    Route route = MaterialPageRoute(builder: (context) => TelaInicial());
    Navigator.pushReplacement(context, route);
  }
}

