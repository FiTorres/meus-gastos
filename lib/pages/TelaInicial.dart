import 'package:flutter/material.dart';
import 'package:flutter_app/models/DialogAdicionar.dart';
import 'package:flutter_app/pages/MeusDesejos.dart';
import 'package:flutter_app/pages/SplashScreen.dart';
import 'package:hive/hive.dart';
import 'package:hive_flutter/hive_flutter.dart';

import 'package:path_provider/path_provider.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';
import 'dart:async';
import 'package:intl/intl.dart';
import 'dart:io';
import 'package:gradient_app_bar/gradient_app_bar.dart';

import 'package:toast/toast.dart';
import '../models/MeusWidgets.dart';

class TelaInicial extends StatefulWidget {
  @override
  _TelaInicialState createState() => _TelaInicialState();
}

class _TelaInicialState extends State<TelaInicial> {
  final List<String> meses = <String>[
    'Janeiro',
    'Fevereiro',
    'Março',
    'Abril',
    'Maio',
    'Junho',
    'Julho',
    'Agosto',
    'Setembro',
    'Outubro',
    'Novembro',
    'Dezembro'
  ];
  final List<String> gastos = <String>[];
  final List<double> valor = <double>[];
  final List<int> tipoGasto = <int>[];

  int ano = 19;
  final List<int> colorCodes = <int>[600, 500, 100];

  double receitas = 0, despesas = 0, saldo = 0;
  String rec, desp, sald, nomeMaiusculo, nome, test = "Van";
  double alturaDaTela, larguraDaTela;
  int _radioValue = 2, navValor=1;


  var isSelected2 = [true, false];
  int tipo=1;

  bool todos = true;

  static var now = new DateTime.now();

  /*String _lista = "";
  List lista2;
  List lista3;
  List lista4;*/

  List mesList;
  Map<dynamic, dynamic> MeusMeses = {
    /*"mes":[
          {
            "id":"",
            "num_mes":"",
            "nome":"",
            "ano":"",
          },
      ]*/
  };

  int anoInicial = int.parse(new DateFormat("yyyy").format(now));
  int mesInicial = int.parse(new DateFormat("MM").format(now)) - 1;
  int anoAtual = int.parse(new DateFormat("yyyy").format(now));
  int mesAtual = int.parse(new DateFormat("MM").format(now)) - 1,
      gastoAtual = 0,
      cont = 1;

      final nomeController = TextEditingController();
      final valorController = TextEditingController();

  //int _radioValue;

  @override
  void initState() {
    super.initState();

    Consultar();
  }

  @override
  Widget build(BuildContext context) {
    alturaDaTela = MediaQuery.of(context).size.height;
    larguraDaTela = MediaQuery.of(context).size.width;
    rec = receitas.toStringAsFixed(2);
    desp = despesas.toStringAsFixed(2);
    sald = saldo.toStringAsFixed(2);
    return Scaffold(
      appBar: GradientAppBar(
    title: Text('Meus Gastos'),
    backgroundColorStart: Color.fromRGBO(224, 49, 40, 1.0),
    backgroundColorEnd: Color.fromRGBO(224, 49, 40, 0.75),
    
    actions: <Widget>[
            // action button
            IconButton(
              icon: Icon(Icons.card_giftcard),
              onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => MeusDesejos()),
            );
              }
            ),
            IconButton(
              icon: Icon(IconData(57669, fontFamily: 'MaterialIcons')),
              onPressed: Adicionar,
            ),
            
          ]),
  
      
      /* AppBar(
          title: Text('Meus Gastos'),
          backgroundColor: Color.fromRGBO(224, 49, 40, 1.0),
          actions: <Widget>[
            // action button
            IconButton(
              icon: Icon(IconData(57669, fontFamily: 'MaterialIcons')),
              onPressed: Adicionar,
            ),
          ]),*/
      body: Stack(fit: StackFit.expand, children: <Widget>[
        Container(
          decoration: BoxDecoration(
            color: new Color(0xff622f74),
            gradient: LinearGradient(
              colors: [
                new Color.fromRGBO(232, 100, 106, 1.0),
                new Color.fromRGBO(232, 100, 0, 1.0)
              ],
              begin: Alignment.centerRight,
              end: Alignment.centerLeft,
            ),
          ),
        ),
        Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          //crossAxisAlignment: CrossAxisAlignment.baseline,
          children: <Widget>[
            HeaderMes(),
            Padding(
              padding: EdgeInsets.only(top: 1.0),
            ),
            Expanded(
              child: ConteudoMes(),
            ),
            Padding(
              padding: EdgeInsets.only(top: 1.0),
            ),
            NavValores(),
          ],
        ),
      ]),
    );
  }

  Widget HeaderMes() {
    if(anoAtual == anoInicial && mesAtual == mesInicial){
      return Container(
      //Mes e botões
      color: Color.fromRGBO(224, 49, 40, 1.5),
      child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.max,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            IconButton(
              icon: const Icon(Icons.keyboard_arrow_left),
              onPressed: Anterior,
              iconSize: 48.0,
              color: const Color(0xFFd91414),
            ),
            Padding(
              padding: EdgeInsets.all(20.0),
            ),
            Expanded(
              child: Text(
                '${meses[mesAtual]}/$ano',
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: Colors.greenAccent,
                  fontSize: 26.0,
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.all(20.0),
            ),
            IconButton(
              icon: const Icon(Icons.keyboard_arrow_right),
              onPressed: Proximo,
              iconSize: 48.0,
              color: const Color(0xFFd91414),
            )
          ]),
    );

    }else{
    return Container(
      //Mes e botões
      color: Color.fromRGBO(224, 49, 40, 1.5),
      child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.max,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            IconButton(
              icon: const Icon(Icons.keyboard_arrow_left),
              onPressed: Anterior,
              iconSize: 48.0,
              color: const Color(0xFFd91414),
            ),
            Padding(
              padding: EdgeInsets.all(20.0),
            ),
            Expanded(
              child: Text(
                '${meses[mesAtual]}/$ano',
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 26.0,
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.all(20.0),
            ),
            IconButton(
              icon: const Icon(Icons.keyboard_arrow_right),
              onPressed: Proximo,
              iconSize: 48.0,
              color: const Color(0xFFd91414),
            )
          ]),
    );
    }
  }

  Widget ConteudoMes() {
    return Container(
      //color: Color.fromRGBO(224, 49, 40, 1.5),
      decoration: BoxDecoration(
        gradient: LinearGradient(
                  colors: [
                    new Color.fromRGBO(232, 100, 106, 1.0),
                    new Color.fromRGBO(232, 100, 0, 1.0)
                  ],
                  begin: Alignment.centerRight,
                  end: Alignment.centerLeft,
                ),
      ),
      height: alturaDaTela * 0.64,
      child: CustomScrollView(
        shrinkWrap: true,
        slivers: <Widget>[
          new SliverList(
            delegate: new SliverChildBuilderDelegate(
              (context, gastoAtual) => new Card(
                child: new Container(
                  /*decoration: new BoxDecoration(
                  gradient: LinearGradient(
                  colors: [
                    new Color.fromRGBO(33, 222, 72, 0.0),
                    new Color.fromRGBO(222, 33, 42, 0.6),
                    //new Color.fromRGBO(33, 222, 72, 0.4),
                  ],
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  stops: [
                  0.85,
                  0.99
                ],
                ),
                  ),*/
                  padding: EdgeInsets.all(10.0),
                  child: new Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      IconPersonalizado(tipoGasto[gastoAtual]),
                      Padding(
                        padding: EdgeInsets.all(10),
                      ),
                      Expanded(
                        child: Text(
                          ' ${gastos[gastoAtual]} \n RS: ${valor[gastoAtual].toStringAsFixed(2)}',
                          textAlign: TextAlign.left,
                          style: TextStyle(
                            color: Colors.black,
                            fontSize: 20.0,
                          ),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.all(10),
                      ),
                      IconButton(
                        alignment: Alignment.centerRight,
                        icon: Icon(Icons.create),
                        tooltip: 'Editar',
                        onPressed: () {
                          setState(() {
                            nome = gastos[gastoAtual];
                            tipo = tipoGasto[gastoAtual];
                            Editar(gastos[gastoAtual], valor[gastoAtual]);
                          });
                        },
                      ),
                      IconButton(
                        alignment: Alignment.centerRight,
                        icon: Icon(Icons.delete),
                        tooltip: 'Excluir',
                        onPressed: () {
                          setState(() {
                            nome = gastos[gastoAtual];
                            Excluir(gastos[gastoAtual]);
                            //
                          });
                        },
                      ),
                    ],
                  ),
                ),
              ),
              childCount: gastos.length,
            ),
          ),
        ],
      ),
    );
  }
  Widget NavValores(){
    if(navValor == 1){
    return Container(
              padding: EdgeInsets.all(1.0),
              decoration: BoxDecoration(
                
              color: Color.fromRGBO(224, 49, 40, 1.5),
                border: Border(top: BorderSide(width: 1.0, color: Colors.amber[900]),),
              ),
              child: Stack(
                children: <Widget>[
                  Column(
                    children: <Widget>[
                      //Butt
                      Container(
                        //padding: EdgeInsets.all(20),
                        color: Color.fromRGBO(224, 49, 40, 1.5),
                        height: alturaDaTela * 0.06,
                        width:  larguraDaTela * 1,
                        //margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
                        child: IconButton(
                        alignment: Alignment.center,
                        icon: Icon(Icons.expand_less),
                        color: Colors.green,
                        iconSize: 40,
                        tooltip: 'Expandir',
                        onPressed: () {
                          setState(() {
                            navValor = 2;
                          });
                        },
                      ),
                      ),
                      
                      Saldo(),
                    ],
                  ),
                ],
              ),
            );
    }else if(navValor == 2){
      return Container(
              padding: EdgeInsets.all(1.0),
              decoration: BoxDecoration(
                
              color: Color.fromRGBO(224, 49, 40, 1.5),
                border: Border(top: BorderSide(width: 1.0, color: Colors.amber[900]),),
              ),
              child: Stack(
                children: <Widget>[
                  Column(
                    children: <Widget>[
                      //Butt
                      Container(
                        padding: EdgeInsets.all(0),
                        color: Color.fromRGBO(224, 49, 40, 1.5),
                        height: alturaDaTela * 0.06,
                        width:  larguraDaTela * 1,
                        child: IconButton(
                        alignment: Alignment.center,
                        icon: Icon(Icons.expand_more),
                        color: Colors.green,
                        iconSize: 40,
                        tooltip: 'Expandir',
                        onPressed: () {
                          setState(() {
                            navValor = 1;
                          });
                        },
                      ),
                      ),
                      Receitas(),
                      Despesas(),
                      Saldo(),
                    ],
                  ),
                ],
              ),
            );
    }
  }

  Widget IconPersonalizado(gasto) {
    if (gasto == 1) {
      return CircleAvatar(
        backgroundColor: Colors.green,
        child: Icon(
          Icons.sentiment_very_satisfied,
          color: Colors.greenAccent[300],
        ),
      );
    } else if (gasto == 2) {
      return CircleAvatar(
        backgroundColor: Colors.red,
        child: Icon(
          Icons.sentiment_very_dissatisfied,
          color: Colors.deepOrange[300],
        ),
      );
    }else{
      return CircleAvatar(
        backgroundColor: Colors.white,
        child: Icon(
          Icons.sentiment_very_dissatisfied,
          color: Colors.white,
        ),
      );
    }
  }

  Widget Saldo() {
    return Container(
      //Mes e botões
      color: Color.fromRGBO(224, 49, 40, 1.5),
      child: Row(
          mainAxisAlignment: MainAxisAlignment.end,
          mainAxisSize: MainAxisSize.max,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              'Saldo:',
              style: TextStyle(color: Colors.white, fontSize: 18.0),
            ),
            RetornaSaldo(),
          ]),
    );
  }
  Widget RetornaSaldo(){
    if(saldo >= 0){
      return Text(
              '  RS: $sald',
              style: TextStyle(color: Colors.greenAccent, fontSize: 18.0),
            );
    }else{
      return Text(
              '  RS: $sald',
              style: TextStyle(color: Colors.red[100], fontSize: 18.0),
            );
    }
  }

  Widget Despesas() {
    return Container(
      //Mes e botões
      color: Color.fromRGBO(224, 49, 40, 1.5),
      child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          mainAxisSize: MainAxisSize.max,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              'Despesas:',
              style: TextStyle(color: Colors.white, fontSize: 18.0),
            ),
            Text(
              '  RS: $desp',
              style: TextStyle(color: Colors.red[100], fontSize: 18.0),
            ),
          ]),
    );
  }

  Widget Receitas() {
    return Container(
      //Mes e botões
      color: Color.fromRGBO(224, 49, 40, 1.5),
      child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          mainAxisSize: MainAxisSize.max,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              'Receitas:',
              style: TextStyle(color: Colors.white, fontSize: 18.0),
            ),
            Text(
              '  RS: $rec',
              style: TextStyle(color: Colors.greenAccent, fontSize: 18.0),
            ),
          ]),
    );
  }

  void Anterior() {
    //Toast.show("Clicou no botão anterior", context, duration: Toast.LENGTH_SHORT, gravity:  Toast.BOTTOM);
    setState(() {
      if (mesAtual == 0) {
        mesAtual = 11;
        anoAtual -= 1;
        ano--;
      } else {
        mesAtual -= 1;
        cont--;
      }
      if (ano < 0) {
        ano = 99;
      }
      Consultar();
    });
  }

  void Proximo() {
    //Toast.show("Clicou no botão próximo", context, duration: Toast.LENGTH_SHORT, gravity:  Toast.BOTTOM);
    setState(() {
      if (mesAtual == 11) {
        mesAtual = 0;
        anoAtual += 1;
        ano++;
      } else {
        mesAtual += 1;
        cont++;
      }
      if (ano > 99) {
        ano = 1;
      }
      Consultar();
    });
  }

  void Adicionar () async {
    Toast.show("Clicou no botão adicionar", context,
        duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
    
    
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = documentsDirectory.path;
    Hive.init(path);
    var box = await Hive.openBox('mes');

    box.put('mes', meses[mesAtual]);
    box.put('ano', anoAtual);
  
    showDialog(
      context: context,
      builder: (_) => DialogAdicionar(),
    );
  }

  void _handleRadioValueChange(int value) {
    //debugPrint("Chegou");
    setState(() {
      _radioValue = value;

      switch (_radioValue) {
        case 1:
          todos = false;
          Excluir(nome);
          //Toast.show("Não repete", context, duration: Toast.LENGTH_SHORT, gravity:  Toast.BOTTOM);
          break;
        case 2:
          todos = true;
          Excluir(nome);
          //Toast.show("Repete", context, duration: Toast.LENGTH_SHORT, gravity:  Toast.BOTTOM);
          break;
      }
      //debugPrint("Radio value dps do switch"+ _radioValue.toString());
    });
  }

  Widget Excluir(String qualGasto) {
    //Toast.show("Clicou no botão excluir: " + qualGasto, context,
    //duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
    nomeMaiusculo = qualGasto.toUpperCase();
    //nome = qualGasto;

    showDialog(
      context: context,
      //builder: (_) => DialogExcluir(),
      builder: (BuildContext context) {
        return Center(
          child: Material(
            color: Colors.transparent,
            child: Container(
                margin: EdgeInsets.all(20.0),
                padding: EdgeInsets.all(15.0),
                height: alturaDaTela * 0.33,
                decoration: ShapeDecoration(
                  gradient: LinearGradient(
                  colors: [
                    new Color.fromRGBO(232, 100, 106, 1.0),
                    new Color.fromRGBO(232, 100, 0, 1.0)
                  ],
                  begin: Alignment.centerRight,
                  end: Alignment.centerLeft,
                ),
                  
                  shape: RoundedRectangleBorder(
                    //side: BorderSide.lerp(a, b, t),
                      borderRadius: BorderRadius.circular(15.0)),
                      
                ),
                child: Column(
                  children: <Widget>[
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.only(
                            top: 10.0, left: 20.0, right: 20.0),
                        child: Text(
                          "$nomeMaiusculo",
                          style: TextStyle(color: Colors.white, fontSize: 20.0),
                        ),
                      ),
                    ),
                    Text(
                      "Deseja realmente excluir ?",
                      style: TextStyle(color: Colors.white, fontSize: 18.0),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Radio(
                          value: 1,
                          groupValue: _radioValue,
                          onChanged: (val) {
                            setState(() {
                              Navigator.of(context).pop();
                              todos = false;
                              _handleRadioValueChange(1);
                            });
                          },
                        ),
                        Text(
                          'Mês atual',
                          style: TextStyle(color: Colors.white),
                        ),
                        Radio(
                          value: 2,
                          groupValue: _radioValue,
                          onChanged: (val) {
                            setState(() {
                              Navigator.of(context).pop();
                              todos = true;
                              _handleRadioValueChange(2);
                            });
                          },
                        ),
                        Text(
                          'Todos',
                          style: TextStyle(color: Colors.white),
                        ),
                      ],
                    ),
                    BotoesFinais(1),
                  ],
                )),
          ),
        );
      },
    );
  }

  Widget BotoesFinais(int condicao) {
    return Expanded(
        child: Row(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.end,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.all(10.0),
          child: ButtonTheme(
              height: 35.0,
              minWidth: 110.0,
              child: RaisedButton(
                color: Colors.white,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(5.0)),
                splashColor: Colors.white.withAlpha(40),
                child: Text(
                  'Voltar',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      color: Colors.orange[900],
                      fontWeight: FontWeight.bold,
                      fontSize: 13.0),
                ),
                onPressed: () {
                  Navigator.of(context).pop();
                  /*setState(() {
                                      Route route = MaterialPageRoute(
                                          builder: (context) => LoginScreen());
                                      Navigator.pushReplacement(context, route);
                                    });*/
                },
              )),
        ),
        Padding(
            padding: const EdgeInsets.only(
                left: 20.0, right: 10.0, top: 10.0, bottom: 10.0),
            child: ButtonTheme(
                height: 35.0,
                minWidth: 110.0,
                child: RaisedButton(
                  color: Colors.white,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(5.0)),
                  splashColor: Colors.white.withAlpha(40),
                  child: Text(
                    'Confirmar',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        color: Colors.orange[900],
                        fontWeight: FontWeight.bold,
                        fontSize: 13.0),
                  ),
                  onPressed: () {
                    setState(() {
                      //excluir
                      if (condicao == 1) {
                        // debugPrint("Excluir e Todos :" + todos.toString());
                        ExecutarExclusao(nome);
                      }
                      //editar
                      else if (condicao == 2) {
                        validaCampos(nome);
                      }
                      /* Route route = MaterialPageRoute(
                                          builder: (context) => LoginScreen());
                                      Navigator.pushReplacement(context, route);
                                   */
                    });
                  },
                ))),
      ],
    ));
  }

  void Editar(String qualGasto, double valorGasto) {
    setState(() {
      int valorInt = valorGasto.toInt();
     valorController.text=valorInt.toString(); 
     nomeController.text=qualGasto.toString(); 
     if (tipo == 1) {
        isSelected2[0] = true;
        isSelected2[1] = false; 
     } else if(tipo == 2) {
        isSelected2[0] = false;
        isSelected2[1] = true; 
      }
     //tipo = tipGasto;
    });
    //debugPrint(tipGasto.toString()+"tipo:"+tipo.toString());
    showDialog(
      context: context,
      //builder: (_) => DialogExcluir(),
      builder: (BuildContext context) {
        return Center(
          child: Material(
            color: Colors.transparent,
            child: Container(
                margin: EdgeInsets.all(20.0),
                padding: EdgeInsets.all(10.0),
                height: alturaDaTela * 0.60,
                decoration: ShapeDecoration(
                  gradient: LinearGradient(
                  colors: [
                    new Color.fromRGBO(232, 100, 106, 1.0),
                    new Color.fromRGBO(232, 100, 0, 1.0)
                  ],
                  begin: Alignment.centerRight,
                  end: Alignment.centerLeft,
                ),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(15.0)),
                ),
                child: Column(
                  children: <Widget>[
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.only(
                            top: 10.0, left: 20.0, right: 20.0),
                        child: Text(
                          "EDITAR",
                          style: TextStyle(color: Colors.white, fontSize: 20.0),
                        ),
                      ),
                    ),
                    Align(
                      alignment: Alignment.topCenter,
                      child: Container(
                        child: Stack(children: <Widget>[
                          Column(children: <Widget>[
                            ToggleButtons(
                              hoverColor: Colors.green,
                              fillColor: retornaCorFill(tipo),
                              color: Colors.red[900],
                              selectedColor: Colors.red[900],
                              borderColor: retornaCorBorder(tipo),
                              selectedBorderColor: retornaCorBorderSelected(tipo),
                              focusColor: Colors.green[100],
                              splashColor: Colors.green[100],
                              borderWidth: 3,
                              disabledColor: Colors.green[900],
                              borderRadius: BorderRadius.circular(15.0),
                              children: [
                                Text('  RECEITA  ',
                                style: TextStyle(color: Colors.white),
                                ),
                                Text(' DESPESA ',
                                style: TextStyle(color: Colors.white),
                                ),
                              ],
                              onPressed: (int index) {
                                setState(() {
                                  for (int buttonIndex = 0; buttonIndex < isSelected2.length; buttonIndex++) {
                                    if (buttonIndex == index) {
                                      isSelected2[buttonIndex] = true;
                                      tipo = 2;
                                    } else {
                                      isSelected2[buttonIndex] = false;
                                      tipo = 1;
                                    }
                                  }
                                  Navigator.of(context).pop();
                                  Editar(qualGasto, valorGasto);
                                });
                              },
                              isSelected: isSelected2,
                            ),
                            TextFormField(
                              autofocus: true,
                              keyboardType: TextInputType.text,
                              style: new TextStyle(
                                  color: Colors.white, fontSize: 20),
                              decoration: InputDecoration(
                                labelText: "Nome:",
                                labelStyle: TextStyle(color: Colors.white),
                              ),
                              controller: nomeController,
                            ),
                            Divider(),
                            TextFormField(
                              autofocus: true,
                              keyboardType: TextInputType.number,
                              style: new TextStyle(
                                  color: Colors.white, fontSize: 20),
                              decoration: InputDecoration(
                                labelText: "Valor:",
                                labelStyle: TextStyle(color: Colors.white),
                              ),
                              controller: valorController,
                            ),
                            SizedBox(height: 10),
                          ]),
                        ]),
                      ),
                    ),
                    BotoesFinais(2),
                  ],
                )),
          ),
        );
      },
    );
  }
  retornaCorFill(int tipo){
        if(tipo == 1){
          return Colors.green[800];
        }else{
          return Colors.red[800];
        }
      }
      retornaCorBorder(int tipo){
        if(tipo == 1){
          return Colors.red[800];
        }else{
          return Colors.green[800];
        }
      }
      retornaCorBorderSelected(int tipo){
        if(tipo == 1){
          return Colors.green[900];
        }else{
          return Colors.red[900];
        }
      }

  void Consultar() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = documentsDirectory.path + "/GastosDB.db";
    var database = await openDatabase(path, version: 3);

    var inner2 = await database.rawQuery(
        'SELECT * FROM gastos_mes INNER JOIN mes ON mes.id_mes = mes_id_mes INNER JOIN gastos ON gastos.id_gastos = gastos_id_gastos WHERE nome_mes = "${meses[mesAtual]}" AND ano = 20$ano');
    //seleciona só os do mes que quiser
    mesList = inner2.toList();

    //mesList.add("teste");
    setState(() {
      gastos.clear();
      valor.clear();
      tipoGasto.clear();
    });

    receitas = 0;
    despesas = 0;
    saldo = 0;

    for (var item in mesList) {
      setState(() {
        gastos.add(item["nome"].toString());
        valor.add(item["valor"]);
        tipoGasto.add(item["tipo"]);
        if (item["tipo"] == 1) {
          receitas += item["valor"];
        } else if (item["tipo"] == 2) {
          despesas += item["valor"];
        }
      });
    }

    await database.close();

    setState(() {
      saldo = receitas - despesas;
      desp = despesas.toStringAsFixed(2);
      rec = receitas.toStringAsFixed(2);
      sald = saldo.toStringAsFixed(2);
    });

    debugPrint(mesList.toString());
    debugPrint(mesAtual.toString());
  }
   void validaCampos(String name){
     if (valorController.text.contains(",")) {
      setState(() {
        valorController.text = valorController.text.replaceAll(new RegExp(r","), ".");
      });
    }
    if(nomeController.text.length <2){
      Toast.show("Nome inválido", context,
        duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
    }
    else if(valorController.text.length < 1 || double.parse(valorController.text) < 1){
          Toast.show("Valor inválido", context,
          duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
    }else{
          ExecutarEdicao(name, nomeController.text, double.parse(valorController.text));
        }
  }

  void ExecutarExclusao(String name) async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = documentsDirectory.path + "/GastosDB.db";
    var database = await openDatabase(path, version: 3);

    var gasto = await database
        .rawQuery("SELECT id_gastos FROM gastos WHERE nome =" + "'$name'");
    //seleciona só os do mes que quiser
    int idGasto;

    List meuGasto = gasto.toList();

    for (var item in meuGasto) {
      setState(() {
        idGasto = item["id_gastos"];
      });
    }
    var atual_Mes = await database.rawQuery(
        "SELECT * FROM mes WHERE nome_mes = '" +
            meses[mesAtual] +
            "' AND ano = " +
            anoAtual.toString() +
            "");

    int atualMes;

    List meuMesAtual = atual_Mes.toList();

    for (var item in meuMesAtual) {
      setState(() {
        atualMes = item["id_mes"];
      });
    }

    debugPrint("Todos: "+todos.toString()+"ID Gasto: "+idGasto.toString());

    if (todos == true) {
      await database.execute(
          " DELETE FROM gastos_mes WHERE gastos_id_gastos = " +
              idGasto.toString());
      await database.execute(
          " DELETE FROM gastos WHERE id_gastos = " + idGasto.toString());
    } else {
      await database.execute(
          " DELETE FROM gastos_mes WHERE gastos_id_gastos = " +
              idGasto.toString() +
              " AND mes_id_mes = " +
              atualMes.toString() +
              "");
    }

    await database.close();

    await Consultar();

    Navigator.of(context).pop();
  }
  void ExecutarEdicao(String name, String nvNome, double nvValor) async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = documentsDirectory.path + "/GastosDB.db";
    var database = await openDatabase(path, version: 3);

    var gasto = await database
        .rawQuery("SELECT id_gastos FROM gastos WHERE nome =" + "'$name'");
    
    //seleciona só os do mes que quiser
    int idGasto;

    List meuGasto = gasto.toList();

    for (var item in meuGasto) {
      setState(() {
        idGasto = item["id_gastos"];
      });
    }
    debugPrint(idGasto.toString());
    String test = "SELECT id_gastos FROM gastos WHERE nome =" + "'$name'";
    debugPrint(test);

    
    await database.execute("UPDATE gastos SET tipo = "+tipo.toString()+", nome = '"+nvNome+"', valor = "+nvValor.toString()+" WHERE id_gastos = "+idGasto.toString());
    

    await database.close();

    await Consultar();

    Navigator.of(context).pop();
  }
}
