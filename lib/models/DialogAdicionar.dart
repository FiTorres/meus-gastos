import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_app/pages/TelaInicial.dart';
import 'package:hive/hive.dart';
import 'package:intl/intl.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';
import 'package:toast/toast.dart';

class DialogAdicionar extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => DialogAdicionarState();
}

class DialogAdicionarState extends State<DialogAdicionar>
    with SingleTickerProviderStateMixin {
  AnimationController controller;
  Animation<double> scaleAnimation;
  double alturaDaTela, tamDig=0.60;
  var isSelected2 = [true, false];
  var isSelected3 = [true, false];
  int tipo = 1, repete = 2;
  int tipo_fixo = 0;
  int _radioValue = 0;
  bool parcelado = false;
  final nome = TextEditingController();
  final valor = TextEditingController();
  final num_Parcelas = TextEditingController();

  static var now = new DateTime.now();
  /*int mesAtual = int.parse(new DateFormat("MM").format(now)) - 1;
  int anoAtual = int.parse(new DateFormat("yyyy").format(now));
  final List<String> meses = <String>[
    'Janeiro',
    'Fevereiro',
    'Março',
    'Abril',
    'Maio',
    'Junho',
    'Julho',
    'Agosto',
    'Setembro',
    'Outubro',
    'Novembro',
    'Dezembro'
  ];*/

  

  @override
  void initState() {
    super.initState();

    controller =
        AnimationController(vsync: this, duration: Duration(milliseconds: 450));
    scaleAnimation =
        CurvedAnimation(parent: controller, curve: Curves.elasticInOut);

    controller.addListener(() {
      setState(() {});
    });

    controller.forward();
  }

  @override
  Widget build(BuildContext context) {
    alturaDaTela = MediaQuery.of(context).size.height;
    return Center(
      child: Material(
        color: Colors.transparent,
        child: ScaleTransition(
          scale: scaleAnimation,
          child: Container(
              margin: EdgeInsets.all(20.0),
              padding: EdgeInsets.all(15.0),
              height: alturaDaTela * tamDig,
              decoration: ShapeDecoration(
                //color: Color.fromRGBO(232, 100, 50, 1.0),
                gradient: LinearGradient(
                  colors: [
                    new Color.fromRGBO(232, 100, 106, 1.0),
                    new Color.fromRGBO(232, 100, 0, 1.0)
                  ],
                  begin: Alignment.centerRight,
                  end: Alignment.centerLeft,
                ),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(15.0),
                ),
              ),
              child: Column(
                children: <Widget>[
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.only(
                          top: 10.0, left: 20.0, right: 20.0),
                      child: Text(
                        "CADASTRO",
                        style: TextStyle(color: Colors.white, fontSize: 20.0),
                      ),
                    ),
                  ),
                  CorpoDialog(),
                  BotoesFinais(),
                ],
              )),
        ),
      ),
    );
  }

  Widget BotoesFinais() {
    return Expanded(
        child: Row(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.end,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.all(10.0),
          child: ButtonTheme(
              height: 35.0,
              minWidth: 110.0,
              child: RaisedButton(
                color: Colors.white,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(5.0)),
                splashColor: Colors.white.withAlpha(40),
                child: Text(
                  'Voltar',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      color: Colors.orange[900],
                      fontWeight: FontWeight.bold,
                      fontSize: 13.0),
                ),
                onPressed: () {
                  Navigator.of(context).pop();
                  /*setState(() {
                                      Route route = MaterialPageRoute(
                                          builder: (context) => LoginScreen());
                                      Navigator.pushReplacement(context, route);
                                    });*/
                },
              )),
        ),
        Padding(
            padding: const EdgeInsets.only(
                left: 20.0, right: 10.0, top: 10.0, bottom: 10.0),
            child: ButtonTheme(
                height: 35.0,
                minWidth: 110.0,
                child: RaisedButton(
                  color: Colors.white,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(5.0)),
                  splashColor: Colors.white.withAlpha(40),
                  child: Text(
                    'Confirmar',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        color: Colors.orange[900],
                        fontWeight: FontWeight.bold,
                        fontSize: 13.0),
                  ),
                  onPressed: () {
                    setState(() {
                      validaCampos();

                      /* Route route = MaterialPageRoute(
                                          builder: (context) => TelaInicial());
                                      Navigator.pushReplacement(context, route);*/
                      //Navigator.of(context).pop();
                    });
                  },
                ))),
      ],
    ));
  }

  Widget CorpoDialog() {
    return Align(
      alignment: Alignment.topCenter,
      child: Container(
        child: Stack(children: <Widget>[
          Column(children: <Widget>[
            ToggleButtons(
              hoverColor: Colors.green,
              fillColor: retornaCorFill(tipo),
              color: Colors.red[900],
              selectedColor: Colors.red[900],
              borderColor: retornaCorBorder(tipo),
              selectedBorderColor: retornaCorBorderSelected(tipo),
              focusColor: Colors.green[100],
              splashColor: Colors.green[100],
              borderWidth: 3,
              disabledColor: Colors.green[900],
              borderRadius: BorderRadius.circular(15.0),
              children: [
                Text(
                  '  RECEITA  ',
                  style: TextStyle(color: Colors.white),
                ),
                Text(
                  ' DESPESA ',
                  style: TextStyle(color: Colors.white),
                ),
              ],
              onPressed: (int index) {
                setState(() {
                  for (int buttonIndex = 0;
                      buttonIndex < isSelected2.length;
                      buttonIndex++) {
                    if (buttonIndex == index) {
                      isSelected2[buttonIndex] = true;
                      tipo = 2;
                    } else {
                      isSelected2[buttonIndex] = false;
                      tipo = 1;
                    }
                  }
                });
              },
              isSelected: isSelected2,
            ),
            TextFormField(
              autofocus: true,
              keyboardType: TextInputType.text,
              style: new TextStyle(color: Colors.white, fontSize: 20),
              decoration: InputDecoration(
                labelText: "Nome:",
                labelStyle: TextStyle(color: Colors.white),
              ),
              controller: nome,
            ),
            Divider(),
            TextFormField(
              autofocus: true,
              keyboardType: 
              TextInputType.numberWithOptions(decimal: true),
              inputFormatters: [BlacklistingTextInputFormatter(new RegExp('[\\-|\\ ]'))],
              style: new TextStyle(color: Colors.white, fontSize: 20),
              decoration: InputDecoration(
                labelText: "Valor:",
                labelStyle: TextStyle(color: Colors.white),
              ),
              controller: valor,
            ),
            SizedBox(height: 10),
            ToggleButtons(
              hoverColor: Colors.green,
              fillColor: Colors.green[800],
              color: Colors.red[900],
              selectedColor: Colors.red[900],
              borderColor: Colors.green[900],
              selectedBorderColor: Colors.green[900],
              focusColor: Colors.green[100],
              splashColor: Colors.green[100],
              disabledColor: Colors.green[900],
              borderWidth: 3,
              borderRadius: BorderRadius.circular(15.0),
              children: [
                Text(
                  ' NÃO REPETE ',
                  style: TextStyle(color: Colors.white),
                ),
                Text(
                  ' REPETE ',
                  style: TextStyle(color: Colors.white),
                ),
              ],
              onPressed: (int index) {
                setState(() {
                  for (int buttonIndex = 0;
                      buttonIndex < isSelected3.length;
                      buttonIndex++) {
                    if (buttonIndex == index) {
                      isSelected3[buttonIndex] = true;
                      repete = 1;
                      tamDig = 0.70;
                    } else {
                      isSelected3[buttonIndex] = false;
                      repete = 2;
                      _radioValue = 1;
                      tipo_fixo = 1;
                      
                      tamDig = 0.60;
                    }
                  }
                });
              },
              isSelected: isSelected3,
            ),
            Repete(),
            Parcelado(),
            /*Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Radio(
                                  value: 0,
                                  groupValue: _radioValue,
                                  onChanged: _handleRadioValueChange,
                                ),
                                Text('Não repete'),
                                new Radio(
                                  value: 1,
                                  groupValue: _radioValue,
                                  onChanged: _handleRadioValueChange,
                                ),
                                new Text('Repete'),
                              ],
                            ),*/
          ]),
        ]),
      ),
    );
  }

  retornaCorFill(int tipo) {
    if (tipo == 1) {
      return Colors.green[800];
    } else {
      return Colors.red[800];
    }
  }

  retornaCorBorder(int tipo) {
    if (tipo == 1) {
      return Colors.red[800];
    } else {
      return Colors.green[800];
    }
  }

  retornaCorBorderSelected(int tipo) {
    if (tipo == 1) {
      return Colors.green[900];
    } else {
      return Colors.red[900];
    }
  }

  void _handleRadioValueChange(int value) {
    setState(() {
      _radioValue = value;

      switch (_radioValue) {
        case 1:
          parcelado = false;
          tipo_fixo = 1;
          //Toast.show("Não repete", context, duration: Toast.LENGTH_SHORT, gravity:  Toast.BOTTOM);
          break;
        case 2:
          parcelado = true;
          tamDig = 0.80;
          tipo_fixo = 2;
          //Toast.show("Repete", context, duration: Toast.LENGTH_SHORT, gravity:  Toast.BOTTOM);
          break;
      }
    });
  }

  Widget Repete() {
    if (isSelected3[1] == true) {
      return Align(
        alignment: Alignment.topCenter,
        child: Container(
          child: Stack(children: <Widget>[
            Column(children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Radio(
                    value: 1,
                    groupValue: _radioValue,
                    onChanged: _handleRadioValueChange,
                  ),
                  Text(
                    'Fixo',
                    style: TextStyle(color: Colors.white),
                  ),
                  new Radio(
                    value: 2,
                    groupValue: _radioValue,
                    onChanged: _handleRadioValueChange,
                  ),
                  new Text(
                    'Parcelado',
                    style: TextStyle(color: Colors.white),
                  ),
                ],
              ),
            ]),
          ]),
        ),
      );
    } else {
      parcelado = false;
      _radioValue = 0;
      tipo_fixo = 0;
      num_Parcelas.text = "0";
      return SizedBox(height: 10);
    }
  }

  Widget Parcelado() {
    if (parcelado == true) {
      return Align(
        alignment: Alignment.topCenter,
        child: Container(
          child: Stack(children: <Widget>[
            Column(children: <Widget>[
              TextFormField(
                autofocus: true,
                keyboardType: TextInputType.number,
                style: new TextStyle(color: Colors.white, fontSize: 20),
                decoration: InputDecoration(
                  labelText: "Numero de Parcelas:",
                  labelStyle: TextStyle(color: Colors.white),
                ),
                controller: num_Parcelas,
              ),
            ]),
          ]),
        ),
      );
    } else {
      num_Parcelas.text = "0";
      return SizedBox(height: 10);
    }
  }

  void validaCampos() {
    if (valor.text.contains(",")) {
      setState(() {
        valor.text = valor.text.replaceAll(new RegExp(r","), ".");
      });
    }
    if (nome.text.length < 2) {
      Toast.show("Nome inválido", context,
          duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
    } else if (valor.text.length < 1 || double.parse(valor.text) < 1) {
      Toast.show("Valor inválido", context,
          duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
    } else {
      Cadastrar();
    }
  }

  void Cadastrar() async {
    //Toast.show("Tipo: "+ tipo.toString() +"  Nome: " + nome.text+ "  Valor: "+valor.text+ "\n  Repete: "+repete.toString()+"  Parcelado:"+tipo_fixo.toString()+" Numero de Parcelas: "+num_Parcelas.text, context, duration: Toast.LENGTH_LONG, gravity:  Toast.BOTTOM);
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = documentsDirectory.path + "/GastosDB.db";
    var database = await openDatabase(path, version: 3);

    Directory documentsDirectoryHive = await getApplicationDocumentsDirectory();
    String pathHive = documentsDirectoryHive.path;
    Hive.init(pathHive);
    var box = await Hive.openBox('mes');

    String mesAtual = box.get('mes');
    int anoAtual = box.get('ano');

    

    //não repete
    if (repete == 2 && tipo_fixo == 0) {
      await database.execute( "INSERT INTO gastos(nome, valor, qtd_parcelas, parcelas_pagas, tipo, fixo) VALUES ('"+nome.text+"', "+valor.text+", 1, 0, "+tipo.toString()+", 0)");
    debugPrint("chegou");
    } else if (repete == 1 && tipo_fixo == 1) {
      await database.execute(
          "INSERT INTO gastos(nome, valor, qtd_parcelas, parcelas_pagas, tipo, fixo) VALUES ('" +
              nome.text +
              "', " +
              valor.text +
              ", 0, 0, " +
              tipo.toString() +
              ", 1)");
    } else if (repete == 1 && tipo_fixo == 2) {
      double ValorParcelas =
          double.parse(valor.text) / int.parse(num_Parcelas.text);
      await database.execute(
          "INSERT INTO gastos(nome, valor, qtd_parcelas, parcelas_pagas, tipo, fixo) VALUES ('" +
              nome.text +
              "', " +
              ValorParcelas.toString() +
              ", " +
              num_Parcelas.text +
              ", 0, " +
              tipo.toString() +
              ", 0)");
    }

    var gasto = await database.rawQuery(
        "SELECT id_gastos FROM gastos WHERE nome = '" + nome.text + "'");
    String teste =
        "SELECT id_gastos FROM gastos WHERE nome = '" + nome.text + "'";
    debugPrint(teste);

    var ultimo_Mes = await database.rawQuery(
        "SELECT * FROM mes WHERE id_mes = (SELECT MAX(id_mes) FROM mes)");

    debugPrint(anoAtual.toString());

    var atual_Mes = await database.rawQuery(
        "SELECT * FROM mes WHERE nome_mes = '" +
            mesAtual +
            "' AND ano = " +
            anoAtual.toString() +
            "");

    //var gasto = await database.rawQuery("SELECT id_gastos FROM gastos WHERE nome = '"+nome.text+"'");

    int idGasto;

    List meuGasto = gasto.toList();

    for (var item in meuGasto) {
      setState(() {
        idGasto = item["id_gastos"];
      });
    }

    int idMes;

    List meuMes = ultimo_Mes.toList();

    for (var item in meuMes) {
      setState(() {
        idMes = item["id_mes"];
      });
    }

    int atualMes;

    List meuMesAtual = atual_Mes.toList();

    for (var item in meuMesAtual) {
      setState(() {
        atualMes = item["id_mes"];
      });
    }

    int nParcelas = int.parse(num_Parcelas.text);

    //não repete
    if (repete == 2) {
      await database.execute(
          "INSERT INTO gastos_mes(mes_id_mes, gastos_id_gastos) VALUES (" +
              atualMes.toString() +
              ", " +
              idGasto.toString() +
              ")");
    } else if (repete == 1 && tipo_fixo == 1) {
      for (int i = 1; i <= idMes; i++) {
        int rep = atualMes + i - 1;
        //debugPrint(i.toString());
        await database.execute(
            "INSERT INTO gastos_mes(mes_id_mes, gastos_id_gastos) VALUES (" +
                rep.toString() +
                ", " +
                idGasto.toString() +
                ")");
      }
    } else if (repete == 1 && tipo_fixo == 2) {
      for (int i = 1; i <= nParcelas; i++) {
        int rep = atualMes + i - 1;
        //debugPrint(rep.toString());
        await database.execute(
            "INSERT INTO gastos_mes(mes_id_mes, gastos_id_gastos) VALUES (" +
                rep.toString() +
                ", " +
                idGasto.toString() +
                ")");
      }
    }
    database.close();

    debugPrint("Repete:" + repete.toString());
    debugPrint("Tipo Fixo:" + tipo_fixo.toString());
    debugPrint("ID Gasto:" + idGasto.toString());
    debugPrint(meuMes.toString());
    debugPrint("ID Mes:" + idMes.toString());

    //var inner2 = await database.rawQuery('SELECT * FROM gastos_mes INNER JOIN mes ON mes.id_mes = mes_id_mes INNER JOIN gastos ON gastos.id_gastos = gastos_id_gastos WHERE nome_mes = "${meses[mesAtual]}" AND ano = 20$ano');
    //seleciona só os do mes que quiser

    debugPrint("Tipo: " +
        tipo.toString() +
        "  Nome: " +
        nome.text +
        "  Valor: " +
        valor.text +
        "\n  Repete: " +
        repete.toString() +
        "  Parcelado:" +
        tipo_fixo.toString() +
        " Numero de Parcelas: " +
        num_Parcelas.text);

    Route route = MaterialPageRoute(builder: (context) => TelaInicial());
    Navigator.pushReplacement(context, route);
  }
}
