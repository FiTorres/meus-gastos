import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_app/pages/MeusDesejos.dart';
import 'package:flutter_app/pages/TelaInicial.dart';
import 'package:intl/intl.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';
import 'package:toast/toast.dart';

class DialogAdicionarDesejos extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => DialogAdicionarDesejosState();
}

class DialogAdicionarDesejosState extends State<DialogAdicionarDesejos>
    with SingleTickerProviderStateMixin {
  AnimationController controller;
  Animation<double> scaleAnimation;
  double alturaDaTela;
  var isSelected2 = [true, false];
  var isSelected3 = [true, false];
  int tipo = 1, repete = 2;
  int valorPrioridade = 0;
  int _radioValue = 0;
  bool parcelado = false;
  final nome = TextEditingController();
  final valor = TextEditingController();


  @override
  void initState() {
    super.initState();

    controller =
        AnimationController(vsync: this, duration: Duration(milliseconds: 450));
    scaleAnimation =
        CurvedAnimation(parent: controller, curve: Curves.elasticInOut);

    controller.addListener(() {
      setState(() {});
    });

    controller.forward();
  }

  @override
  Widget build(BuildContext context) {
    alturaDaTela = MediaQuery.of(context).size.height;
    return Center(
      child: Material(
        color: Colors.transparent,
        child: ScaleTransition(
          scale: scaleAnimation,
          child: Container(
              margin: EdgeInsets.all(20.0),
              padding: EdgeInsets.all(20.0),
              height: alturaDaTela * 0.60,
              decoration: ShapeDecoration(
                //color: Color.fromRGBO(232, 100, 50, 1.0),
                gradient: LinearGradient(
                  colors: [
                    new Color.fromRGBO(232, 100, 106, 1.0),
                    new Color.fromRGBO(232, 100, 0, 1.0)
                  ],
                  begin: Alignment.centerRight,
                  end: Alignment.centerLeft,
                ),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(15.0),
                ),
              ),
              child: Column(
                children: <Widget>[
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.only(
                          top: 10.0, left: 20.0, right: 20.0),
                      child: Text(
                        "CADASTRO",
                        style: TextStyle(color: Colors.white, fontSize: 20.0),
                      ),
                    ),
                  ),
                  CorpoDialog(),
                  BotoesFinais(),
                ],
              )),
        ),
      ),
    );
  }

  Widget BotoesFinais() {
    return Expanded(
        child: Row(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.end,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.all(10.0),
          child: ButtonTheme(
              height: 35.0,
              minWidth: 110.0,
              child: RaisedButton(
                color: Colors.white,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(5.0)),
                splashColor: Colors.white.withAlpha(40),
                child: Text(
                  'Voltar',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      color: Colors.orange[900],
                      fontWeight: FontWeight.bold,
                      fontSize: 13.0),
                ),
                onPressed: () {
                  Navigator.of(context).pop();
                  /*setState(() {
                                      Route route = MaterialPageRoute(
                                          builder: (context) => LoginScreen());
                                      Navigator.pushReplacement(context, route);
                                    });*/
                },
              )),
        ),
        Padding(
            padding: const EdgeInsets.only(
                left: 20.0, right: 10.0, top: 10.0, bottom: 10.0),
            child: ButtonTheme(
                height: 35.0,
                minWidth: 110.0,
                child: RaisedButton(
                  color: Colors.white,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(5.0)),
                  splashColor: Colors.white.withAlpha(40),
                  child: Text(
                    'Confirmar',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        color: Colors.orange[900],
                        fontWeight: FontWeight.bold,
                        fontSize: 13.0),
                  ),
                  onPressed: () {
                    setState(() {
                      validaCampos();

                      /* Route route = MaterialPageRoute(
                                          builder: (context) => TelaInicial());
                                      Navigator.pushReplacement(context, route);*/
                      //Navigator.of(context).pop();
                    });
                  },
                ))),
      ],
    ));
  }

  Widget CorpoDialog() {
    return Align(
      alignment: Alignment.topCenter,
      child: Container(
        child: Stack(children: <Widget>[
          Column(children: <Widget>[
            TextFormField(
              autofocus: true,
              keyboardType: TextInputType.text,
              style: new TextStyle(color: Colors.white, fontSize: 20),
              decoration: InputDecoration(
                labelText: "Nome:",
                labelStyle: TextStyle(color: Colors.white),
              ),
              controller: nome,
            ),
            Divider(),
            TextFormField(
              autofocus: true,
              keyboardType: 
              TextInputType.numberWithOptions(decimal: true),
              inputFormatters: [BlacklistingTextInputFormatter(new RegExp('[\\-|\\ ]'))],
              style: new TextStyle(color: Colors.white, fontSize: 20),
              decoration: InputDecoration(
                labelText: "Valor:",
                labelStyle: TextStyle(color: Colors.white),
              ),
              controller: valor,
            ),
            SizedBox(height: 10),
            Repete(),
          ]),
        ]),
      ),
    );
  }

  void _handleRadioValueChange(int value) {
    setState(() {
      _radioValue = value;

      switch (_radioValue) {
        case 1:
          valorPrioridade = 1;
          //Toast.show("Não repete", context, duration: Toast.LENGTH_SHORT, gravity:  Toast.BOTTOM);
          break;
        case 2:
          valorPrioridade = 2;
          //Toast.show("Repete", context, duration: Toast.LENGTH_SHORT, gravity:  Toast.BOTTOM);
          break;
        case 3:
          valorPrioridade = 3;
          //Toast.show("Repete", context, duration: Toast.LENGTH_SHORT, gravity:  Toast.BOTTOM);
          break;
      }
    });
  }

  Widget Repete() {
      return Align(
        alignment: Alignment.topCenter,
        child: Container(
          child: Stack(children: <Widget>[
            Column(children: <Widget>[
              
            SizedBox(height: 10),

              Text(
                    'PRIORIDADE',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                        fontSize: 13.0),
                  ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Radio(
                    value: 1,
                    groupValue: _radioValue,
                    onChanged: _handleRadioValueChange,
                  ),
                  Text(
                    'Baixo',
                    style: TextStyle(color: Colors.white),
                  ),
                  new Radio(
                    value: 2,
                    groupValue: _radioValue,
                    onChanged: _handleRadioValueChange,
                  ),
                  new Text(
                    'Medio',
                    style: TextStyle(color: Colors.white),
                  ),
                  new Radio(
                    value: 3,
                    groupValue: _radioValue,
                    onChanged: _handleRadioValueChange,
                  ),
                  new Text(
                    'Alto',
                    style: TextStyle(color: Colors.white),
                  ),
                ],
              ),
            ]),
          ]),
        ),
      );
    }

  void validaCampos() {
    if (valor.text.contains(",")) {
      setState(() {
        valor.text = valor.text.replaceAll(new RegExp(r","), ".");
      });
    }
    if (nome.text.length < 2) {
      Toast.show("Nome inválido", context,
          duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
    } else if (valor.text.length < 1 || double.parse(valor.text) < 1) {
      Toast.show("Valor inválido", context,
          duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
    } else if(valorPrioridade <1 || valorPrioridade >3){
      Toast.show("Informe um nível de prioridade", context,
          duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
    }
    else {
      Cadastrar();
    }
    
  }

  void Cadastrar() async {
    //Toast.show("Tipo: "+ tipo.toString() +"  Nome: " + nome.text+ "  Valor: "+valor.text+ "\n  Repete: "+repete.toString()+"  Parcelado:"+valorPrioridade.toString()+" Numero de Parcelas: "+num_Parcelas.text, context, duration: Toast.LENGTH_LONG, gravity:  Toast.BOTTOM);
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = documentsDirectory.path + "/GastosDB.db";
    var database = await openDatabase(path, version: 3);

    //não repete
    
      await database.execute( "INSERT INTO desejos(nome_desejos, valor_desejos, prioridade) VALUES ('"+nome.text+"', "+valor.text+", "+valorPrioridade.toString()+")");
    

    database.close();

    
    //var inner2 = await database.rawQuery('SELECT * FROM gastos_mes INNER JOIN mes ON mes.id_mes = mes_id_mes INNER JOIN gastos ON gastos.id_gastos = gastos_id_gastos WHERE nome_mes = "${meses[mesAtual]}" AND ano = 20$ano');
    //seleciona só os do mes que quiser

    debugPrint("Tipo: " +
        tipo.toString() +
        "  Nome: " +
        nome.text +
        "  Valor: " +
        valor.text +
        " Prioridade: " +
        valorPrioridade.toString());

    Route route = MaterialPageRoute(builder: (context) => MeusDesejos());
    Navigator.pushReplacement(context, route);
  }
}
